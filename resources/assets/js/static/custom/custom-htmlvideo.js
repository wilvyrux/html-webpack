$(document).ready(function() {
    var ctrlVideo = document.getElementById("video");

    $('.welcome-section__play').click(function() {
        if ($('.welcome-section__play').hasClass("active")) {

            ctrlVideo.play();

            // $('.video-box__play').html("Pause");
            $('.welcome-section__play').toggleClass("active");
            $('.welcome-section__play__item').addClass('welcome-section__play__item--active');
        } else {

            ctrlVideo.pause();

            // $('.video-box__play').html("play");
            $('.welcome-section__play').toggleClass("active");
            $('.welcome-section__item').removeClass('welcome-section__item--active');
        }
    });

});